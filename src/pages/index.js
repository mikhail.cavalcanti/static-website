import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Pudim é bom</h1>
    <h2>Pudim é muito bom</h2>
    <h2>É sério...</h2>
    <h2>É maravilhoso</h2>
    <p>Bem vindo ao site do pudim</p>
    <p>Esse commit vai ser rejeitado!</p>
    <p>Agora vá comer um pudim!</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go to page 2</Link>
  </Layout>
)

export default IndexPage
