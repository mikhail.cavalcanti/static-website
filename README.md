# Projeto de estudo de pipeline do gitlab

Este repositório tem como objetivo o estudo de pipeline usando o gitlab, o projeto (código) é um site estático gerado com o [gatsby](https://www.gatsbyjs.org/) e publicado usando o [surge](https://surge.sh/).

Características projeto: 

- O branch master está bloqueado para commit, aceitando apenas merge request.
- Existe uma pipeline para o branch master, outra para qualquer outro branch criado, de feature por exemplo, e mais um pipeline agendado para atualizar o cache do npm quando busca as dependências ao executar o comando `npm install`

# TLDR

- Quer disparar a pipeline? Faça um clone do projeto, crie um branch, altere o código, faça o commit e em seguida o push, isto já é o suficiente para disparar uma pipeline e ter uma url pública (**handsome-`$CI_ENVIRONMENT_SLUG`.surge.sh**)
  - Mais explicações nos tópicos **Rodando o projeto de forma local**, **Pipeline de qualquer outro branch** e **Ambientes**
- Para disparar a pipeline no master faça um pull request para o branch master, uma vez que este seja aceito o pipeline do master que irá atualizar o ambiente de produção. A descrição deste pipeline está no tópico **Pipeline do branch master**. No momento apenas quem tem perfil de **Maintainer** pode aceitar o pull request.

### Pipeline do branch master

Ao aceitar um merge request é disparada a pipeline com as seguintes etapas (steps)

- build -> constroi os artefatos a serem testados e publicados
- test -> testa se os artefatos foram gerados corretamente e levanta o server local para mais testes
- deploy staging -> entrega os artefatos no ambiente de stating ([url](handsome-staging.surge.sh))
- deploy production -> entrega os artefatos no ambiente de produção ([url](handsome-veil.surge.sh))
- production tests -> testa se foi publicado corretamente em produção
- Após esse pipeline concluir, é executado o job **stop review**
  - sobre o job **stop review**
    - ele não está no pipeline do branch master, está no pipeline do branch que requisitou o merge request
    - tem como objetivo retira do ar o domínio que foi publicado para testar o branch que está sendo mergeado

### Pipeline de qualquer outro branch

Após clonar o projeto, criar um branch, realizar um commit e push do mesmo, é disparada a pipeline com as seguintes etapas (steps)

- build -> constroi os artefatos a serem testados e publicados
- test -> testa se os artefatos foram gerados corretamente e levanta o server local para mais testes
- deploy review -> entrega os artefatos no ambiente exclusivo para este branch com a url no seguinte modelo **handsome-`$CI_ENVIRONMENT_SLUG`.surge.sh**, para ver as urls disponíveis dos ambientes veja o tópico **Ambientes**
- stop review -> remove o domínio publicado no [surge](https://surge.sh/), disparado apenas depois que o merge request é aceito no branch master e sua pipeline é concluída com sucesso

# Rodando o projeto de forma local

Enquanto desenvolvedor que deseja modificar o código do projeto, após fazer o clone do projeto e criar um novo branch, basta executar o `npm install`, `npm install -g gatsby-cli` e `gatsby serve` com isso o projeto estará rodando na url http://localhost:9000

# Ambientes

Existem dois ambientes fixos e um dinâmico, os fixos são **staging** e **production**, e o dinâmico é criado e apagado automáticamente para cada branch que for disparado um pipeline, o nome deste ambiente é **review/`$CI_COMMIT_REF_NAME`**.

Para ver os ambientes vá no menu lateral esquerdo, na opção **Operations > Environments**, ([ou clique aqui](https://gitlab.com/mikhail.cavalcanti/static-website/-/environments)), na linha de cada ambiente se o pipeline já foi disparado e concluído, ao lado esquerdo do botão de stop aparecerá um botão que leva ao link do ambiente que foi publicado, no caso do ambiente dinâmico é bom de ver a url aqui.

# Informações Sensíveis

A ferramenta utilizada para o delivery ([surge](https://surge.sh/)) utiliza credenciais de acesso para utilização de seus serviços, para que estas informações não fiquem expostas no arquivo de configuração **gitlab-ci.yml** foram criadas duas variáveis de ambiente que guardam estes valores, são elas **SURGE_LOGIN** e **SURGE_TOKEN**. Estas encontram-se no menu lateral esquerdo **Settings -> CI / CD -> Variables**. Desta forma as variáveis de ambiente ficam disponíveis no container e o surge sabe encontrar-las e utilizalas.

# Pipeline Agendado

Como estratégia de diminuir o tempo da pipeline com uso de cache ao executar o `npm install` foi feito um agendamento de pipeline que tem o único propósito de atualizar este cache. Este foi configurado para rodar uma vez ao dia no menu lateral esquerdo **CI / CD -> Schedules**.
